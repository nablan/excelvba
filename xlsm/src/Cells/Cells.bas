Attribute VB_Name = "Cells"
Option Explicit

' シートの全セルを正方形にして方眼紙にする（リボンからの実行用）
' @param control コントロール
Sub GridForRibbon(ByVal control As IRibbonControl)
    Call Grid
End Sub

' シートの全セルを正方形にして方眼紙にする
Sub Grid()
    ' 設定する方眼紙のサイズ(単位：ポイント）
    Dim point As Double
    
    ' 現在選択中のセルの高さを方眼紙のサイズとする
    point = Selection(1).height
    
    ' 方眼紙を設定
    Call SetGridCells(point)
End Sub

' シートの全セルを指定したサイズの正方形（方眼紙）にする
' @param point 方眼紙のサイズ（単位：ポイント）
Sub SetGridCells(ByVal point As Double)
    ' 列幅（単位：文字数）
    Dim widthChar As Double
    ' Excelに設定可能なポイント
    Dim pointExcel As Double
    
    ' Excelに設定可能なポイント数を計算
    pointExcel = CalcExcelPoint(point)
    ' 列幅（単位：文字数）を取得
    widthChar = PointToCharCount(pointExcel)
    
    ' 全てのセルの列と幅を設定
    Call SetAllCellSize(widthChar, pointExcel)
    
    Range("A1").Select
End Sub

' 全てのセルの幅と高さを設定する
' @param width セルの幅
' @param height セルの高さ
Sub SetAllCellSize(ByVal width As Double, ByVal height As Double)
    Columns.ColumnWidth = width
    Rows.RowHeight = height

    'Debug.Print ("Width=" & Range("A1").width & ", Height=" & Range("A1").height & ", ColumnWidth=" & Range("A1").ColumnWidth & ", RowHeight=" & Range("A1").RowHeight)

End Sub

' 引数の任意のポイント数からExcelに設定可能なポイント数を返す。
' 設定可能なポイント数は、引数のポイント数を超えない最大の値とする。
' @param point ポイント数
' @return Excelに設定可能なポイント数
Private Function CalcExcelPoint(ByVal point As Double)
    ' 1ピクセルのポイントでの長さ（最小単位）
    Const BASE_POINT As Double = 0.75
    
    ' Excelで設定できるポイント数に変換する
    ' 元のポイントを超えない最大の値とする
    CalcExcelPoint = Int(point / BASE_POINT) * BASE_POINT
    
End Function

' 列幅(単位：ポイント)からColumnWidthに設定する列幅(単位：文字数)を計算して返す。
' @param point 列幅(Width)に設定したい値(単位：ポイント)
' @return 列幅(ColumnWidth)に設定する値(単位：文字数)
Private Function PointToCharCount(ByVal point As Double)
    ' ColumnWidthに設定する列幅はExcelのデフォルトフォントに設定されたフォントで
    '「0」の文字が何文字分の幅になるかを指定する。
    ' そのためColumnWidthに設定すべき値はフォントに依存して変わるため、
    ' 実際にColumnWidthに値を設定してWidthの値を取得することで列幅（文字数）を決定する。
    ' Widthは単位がポイントであるがRead Onlyのため、このような方法を使用する
    
    ' 列幅設定に使用するセル
    Dim operationCell As Range
    ' 初期の列幅
    Dim initWidth As Double
    ' 初期のApplication.ScreenUpdating
    Dim initScreenUpdating As Boolean
    ' 処理中の変更前の列幅（退避用）
    Dim beforeColumnWidth As Double
    
    ' どのセルを使用しても良いが仮にA1セルを使用するものとする
    Set operationCell = Range("A1")
    
    ' 列幅を変更するため画面の表示を止める
    initScreenUpdating = Application.ScreenUpdating
    Application.ScreenUpdating = False
    
    With operationCell
        ' 変更前の列幅を退避
        initWidth = .ColumnWidth
        
        '列幅が0（非表示）の場合に計算処理ができないため初期値を設定
        If .ColumnWidth = 0 Then
            .ColumnWidth = 0.1
        End If
        
        ' プロポーショナルフォントの場合は、文字数と幅が等倍で比例しないため
        ' 1度ではColumnWidthが正しく設定されないためループする
        Do
            ' 現在の列幅を取得
            beforeColumnWidth = .ColumnWidth
            
            ' 指定ポイント数に幅を変更
            ' ポイントに現在のColumnWidthとWidthの比率を掛けて文字数を計算する
            .ColumnWidth = point * beforeColumnWidth / .width
            
            'Debug.Print ("BeforeColumnWidth=" & beforeColumnWidth & ", AfterColumnWidth=" & .ColumnWidth & ", AfterWidth=" & .width)
            
            ' 設定前と後が同じになる（収束する）まで計算を繰り返す
        Loop While .ColumnWidth <> beforeColumnWidth
        
        ' 収束した列幅を返す
        PointToCharCount = .ColumnWidth
        
        ' 列幅を元に戻す
        .ColumnWidth = initWidth
    End With
    
    ' 画面の表示を元に戻す
    Application.ScreenUpdating = initScreenUpdating
    
    ' 後処理
    Set operationCell = Nothing
End Function
